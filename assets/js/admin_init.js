$(document).ready(function(){ 
    $("#submit").click(function(){
        var name=$('#name').val();
        var pass=$('#pass').val();
        $.ajax({
            type:'post',
            url:'adminError.php',
            data:{ajax_name:name,ajax_pass:pass},
            success:function(x){
                if(x=="1"){
                        window.location.href="about.php";
                }else{
                        $("#admin_error").text("Please enter the correct username and password !!!");
                };
            }
        })
    });
    $(document).keypress(function(event){
        if(event=='13'){
            $("#submit").trigger("click");
        }
    });
    $(".edit").dblclick(function(){
        $("#popup").addClass("block");
        var members_id=$(this).find('img').attr('name');
        var src=$(this).children().attr('src');
        var name=$(this).find('span').text();
        var text=$(this).find('textarea').text();
        $(".zoom_img").attr('src',src);
        $("#edit_name").val(name);
        $("#edit_text").text(text);
        $("#edit_img").attr('name', members_id);
    });
    $("#zoomed").click(function(a){
        a.stopPropagation();
    });
    $("#popup").click(function(){
        $("#popup").removeClass("block");
    });
    $("#close_zoom").click(function(){
        $("#popup").removeClass("block");
    });
////////////////////////////////////////////
    $("#edit").click(function(){
        var edit_file= $('#edit_file').val();
        var edit_name= $('#edit_name').val();
        var edit_text= $("#edit_text").val();
        var edit_id=$("#edit_img").attr('name');
		var data = new FormData();
		data.append('ajax_edit_name', edit_name);
		data.append('ajax_edit_text', edit_text);
		data.append('ajax_edit_id', edit_id);
		data.append('edit_file', $('#edit_file')[0].files[0]);
        $.ajax({   
            dataType:'json',
            type:"post",
            url:'edit_members.php',
            data: data,
            contentType: false,
            processData: false,
            success:function(result){
                $( "img[name="+result['id']+"]" ).next().next().text(result['eng_title'] );
                $( "img[name="+result['id']+"]" ).parent().find('textarea').text(edit_text);
                $( "img[name="+result['id']+"]" ).parent().find('img').attr('src','../uploads/'+$('#edit_file').val().replace(/C:\\fakepath\\/i, ''));
            }
        })
    })
        

});




