<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacts extends CI_Controller {

	public function index()
	{
		$lang = $this->session->userdata('site_lang');

		$this->lang->load('menu', $lang);
		$menu['items'] = $this->lang->line('menu');
		$menu['current'] = $menu['items']['contacts'];

		$this->load->view('layout/header', $menu);
		$this->load->view('contacts');
		$this->load->view('layout/footer');
	}
}
