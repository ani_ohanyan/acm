<?php
class LangSwitch extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
    }

    /**
     * @param string $language
     */
    function switchLanguage($language = "") {
        $language = ($language != "") ? $language : DEFAULT_LANGUAGE;
        $this->session->set_userdata('site_lang', $language);
        redirect( $_SERVER['HTTP_REFERER']);
    }
}