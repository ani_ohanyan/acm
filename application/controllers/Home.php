<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$lang = $this->session->userdata('site_lang');
		$this->lang->load('about', $lang);
		$data['about'] = $this->lang->line('about');

        $this->lang->load('menu', $lang);
        $menu['items'] = $this->lang->line('menu');
		$menu['current'] = $menu['items']['home'];

		$this->load->view('layout/header', $menu);
		$this->load->view('home', ['data' => $data]);
		$this->load->view('layout/footer');
	}
    
    
}
