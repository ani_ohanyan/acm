<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	public function index()
	{
		$lang = $this->session->userdata('site_lang');

		$this->lang->load('menu', $lang);
		$menu['items'] = $this->lang->line('menu');
		$menu['current'] = $menu['items']['gallery'];

		$this->load->view('layout/header', $menu);
		$this->load->view('gallery');
		$this->load->view('layout/footer');
	}



}
