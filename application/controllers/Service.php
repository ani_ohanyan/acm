<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service extends CI_Controller {

	public function index()
	{
		$lang = $this->session->userdata('site_lang');

		$this->lang->load('menu', $lang);
		$menu['items'] = $this->lang->line('menu');
		$menu['current'] = $menu['items']['service'];

		$this->load->view('layout/header', $menu);
		$this->load->view('service', ['data' => 1]);
		$this->load->view('layout/footer');
	}
}
