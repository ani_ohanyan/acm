<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	public function index()
	{
		$lang = $this->session->userdata('site_lang');
		$this->lang->load('about', $lang);
		$data = $this->lang->line('about');

		$this->lang->load('menu', $lang);
		$menu['items'] = $this->lang->line('menu');
		$menu['current'] = $menu['items']['about'];

		$this->load->view('layout/header', $menu);
		$this->load->view('about', ['data' => $data]);
		$this->load->view('layout/footer');
	}
}
