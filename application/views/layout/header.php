<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>ACM Transporti</title>
        <link rel="shortcut icon" type="image/ico" href="<?= base_url();?>assets/img/favicon.ico"/>
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/font-awesome.min.css"/>
        <link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css"/>

        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
    </head>
    <body>
    <div style="overflow: auto">
        <!-----------MOBILE MENU----------->
        <!-- Latest compiled and minified JavaScript -->
        <nav id="myNavmenu" class="navmenu navmenu-default navmenu-fixed-left offcanvas" role="navigation">
            <a class="navmenu-brand" href="#">
                <img src="<?=base_url()?>assets/img/logo.png" class="col-lg-3 col-md-3 col-xs-5 no_padding" title="ACM">
            </a>
            <ul class="nav navmenu-nav">
                <?php foreach($items as $key => $item){?>
                    <li class="<?=$current==$item?"active":''?>">
                        <a href="<?=base_url($key)?>">
                            <?=$item?>
                        </a>
                    </li>
                <?php }?>
            </ul>
        </nav>
        <div class="navbar navbar-default navbar-fixed-top">
            <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target="#myNavmenu" data-canvas="body">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-----------LANGUAGES----------------->
        <div class="lang_section">
            <select class="lang_select" title="Choose one of the following..."  onchange="javascript:window.location.href='<?php echo base_url(); ?>langSwitch/switchLanguage/'+this.value;">
                <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"'; ?>>English</option>
                <option value="italian" <?php if($this->session->userdata('site_lang') == 'italian') echo 'selected="selected"'; ?>>Italian</option>
            </select>
        </div>
        <!-----------LOGO----------------->

        <section id="logo_container" classs="col-lg-12">
            <a href="<?=base_url()?>">
                <img src="<?=base_url()?>assets/img/logo.png" class="col-lg-offset-4 col-lg-3 col-md-3 col-xs-offset-4 col-xs-4" title="ACM logo">
            </a>
        </section>
        <!-----------MENU----------------->
<!--        <div class="container">-->
            <div id="menu_all">
                <section id="menu_container" class="col-lg-12 col-sm-12 col-xs-12" >
                    <div id="menu">
                        <?php foreach($items as $key => $item){?>
                            <div class="menu_items <?=$key=='home'?'col-lg-offset-1 col-sm-offset-1 col-xs-offset-0':''?>">
                                <a href="<?=base_url($key)?>">
                                    <div class="menu_elem" id="<?=$current==$item?"active_menu":''?>" style="overflow:hidden; <?=$key=='contacts'?'border-right:0px solid #aaa':''?>">
                                        <span class="menu_text_hover">
                                            <span class="menu_hover">
                                                <?=$item?>
                                            </span>
                                        </span>
                                        <span class="menu_text"><?=$item?></span>
                                    </div>
                                </a>
                            </div>
                        <?php }?>
                        <div class="col-lg-1 col-sm-1 col-xs-0" style="padding: 0"></div>
                    </div>
                </section>
            </div>
    <!------------- TOP --------------->
        <div id="top"> </div>