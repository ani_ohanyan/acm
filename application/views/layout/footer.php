
		<!-- Modal -->
		<div id="imgModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Modal Header</h4>
					</div>
					<div class="modal-body">
						<img class="zoom_img" width='100%' src="" />
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		</div>
		<footer id="footer_container">
			<div class="col-lg-12">
				<div class="center">
					<p id="footer_text"> SUBSCRIBE TO OUR NEWSLETTER TO RECEIVE NEWS, <br> UPDATES, AND ANOTHER STUFF BY EMAIL. </p>
				</div>
				<div id="contacts">
					<span>ACM Transporti srl </span>
					<span> &#169; <?=date('Y')?> | </span>
					<a href="#" id="private">Privacy Policy</a>
					<div>
						<span class="soc_hover">
							<span class="social" id="fb"> </span>
							<span class="soc_name"> facebook </span>
						</span>
						<span class="soc_hover">
							<span class="social" id="rss"></span>
							<span class="soc_name" style="left:8px;"> rss </span>
						</span>
						<span class="soc_hover">
							<span class="social" id="twitter"> </span>
							<span class="soc_name" style="left:0px;"> twitter </span>
						</span>
						<span class="soc_hover">
							<span class="social" id="google"> </span>
							<span class="soc_name" style="left:0px;"> google </span>
						</span>
					</div>
				</div>
            </div>
        </footer>

<script src="<?=base_url()?>assets/js/jquery.min.js"></script>
<script src="<?=base_url()?>assets/js/jquery.nicescroll.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="<?=base_url()?>assets/js/init.js"></script>
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>

</body>
</html>