<div class="center col-lg-10 col-lg-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
    <h1 class="modern_title" style="margin-left:0">
        <?=$data?>
    </h1>
    <div id="about_part1">
        <img src="<?=base_url()?>assets/img/about.jpg" class="float_l col-lg-4"/>
        <div class="float_l col-lg-8" id="about_text">
            <h3>About Our Company</h3>
            <p>
                Lorem Ipsum è un testo segnaposto utilizzato nel settore della tipografia e della stampa. Lorem Ipsum è considerato il testo segnaposto standard sin dal sedicesimo secolo, quando un anonimo tipografo prese una cassetta di caratteri e li assemblò per preparare un testo campione. È sopravvissuto non solo a più di cinque secoli, ma anche al passaggio alla videoimpaginazione, pervenendoci sostanzialmente inalterato. Fu reso popolare, negli anni ’60, con la diffusione dei fogli di caratteri trasferibili “Letraset”, che contenevano passaggi del Lorem Ipsum, e più recentemente da software di impaginazione come Aldus PageMaker, che includeva versioni del Lorem Ipsum.
                <span id="dots"> ... </span>
            </p>
            <div id="more_info_about1">
                <div class="more_info" id="more_about">	
                    <span> MORE INFO </span>
                </div>
            </div>	
        </div>	
    </div>
</div>
<!------------------------------------------------->
<div id="work_team_container">
    <div class="col-lg-10 col-lg-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 no_padding">
        <div class="center" id="work_team">
            <h1 class="modern_title" style="margin-left:0"> WORK TEAM </h1>
            <div class="cikl">
                <div class="col-lg-3 col-md-6 col-xs-6 float_l">
                    <div class="modern_img zoom modern_img2"data-toggle="modal" data-target="#imgModal">
                        <img src="<?=base_url()?>assets/img/img_about_1.jpg" name='.$row_memb["id"].' />
                    </div>
                    <div class="workers_info">
                        <a href="#" class="worker_name">
                            Name Surname
                        </a>
                        <p class="about_worker">
                            Position
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-xs-6 float_l">
                    <div class="modern_img zoom modern_img2" data-toggle="modal" data-target="#imgModal">
                    <img src="<?=base_url()?>assets/img/img_about_2.jpg" name='.$row_memb["id"].' />
                    </div>
                    <div class="workers_info">
                        <a href="#" class="worker_name">
                            First Name Last Name
                        </a>
                        <p class="about_worker">
                            Position
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-xs-6 float_l">
                    <div class="modern_img zoom modern_img2" data-toggle="modal" data-target="#imgModal">
                    <img src="<?=base_url()?>assets/img/img_about_3.jpg" name='.$row_memb["id"].' />
                    </div>
                    <div class="workers_info">
                        <a href="#" class="worker_name">
                            First Name Last Name
                        </a>
                        <p class="about_worker">
                            Position
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-xs-6 float_l">
                    <div class="modern_img zoom modern_img2" data-toggle="modal" data-target="#imgModal">
                    <img src="<?=base_url()?>assets/img/img_about_4.jpg" name='.$row_memb["id"].' />
                    </div>
                    <div class="workers_info">
                        <a href="#" class="worker_name">
                            First Name Last Name
                        </a>
                        <p class="about_worker">
                            Position
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div id="more_info_about">
            <img src="<?=base_url()?>assets/img/load.gif" id="loading">
            <div class="more_info" id="show_more">
                <span> SHOW MORE </span>
            </div>
        </div>
    </div>

</div>