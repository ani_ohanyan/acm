<link rel="stylesheet" href="<?= base_url() ?>assets/css/slider_css.css"/>
<script src="<?=base_url()?>assets/js/slider/jssor.slider-22.2.16.min.js" type="text/javascript"></script>
<script type="text/javascript">
    jssor_1_slider_init = function() {

        var jssor_1_SlideshowTransitions = [
            {$Duration:1200,$Zoom:1,$Easing:{$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$OutQuad},$Opacity:2},
            {$Duration:1000,$Zoom:11,$SlideOut:true,$Easing:{$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear},$Opacity:2},
            {$Duration:1200,$Zoom:1,$Rotate:1,$During:{$Zoom:[0.2,0.8],$Rotate:[0.2,0.8]},$Easing:{$Zoom:$Jease$.$Swing,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$Swing},$Opacity:2,$Round:{$Rotate:0.5}},
            {$Duration:1000,$Zoom:11,$Rotate:1,$SlideOut:true,$Easing:{$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InExpo},$Opacity:2,$Round:{$Rotate:0.8}},
            {$Duration:1200,x:0.5,$Cols:2,$Zoom:1,$Assembly:2049,$ChessMode:{$Column:15},$Easing:{$Left:$Jease$.$InCubic,$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
            {$Duration:1200,x:4,$Cols:2,$Zoom:11,$SlideOut:true,$Assembly:2049,$ChessMode:{$Column:15},$Easing:{$Left:$Jease$.$InExpo,$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear},$Opacity:2},
            {$Duration:1200,x:0.6,$Zoom:1,$Rotate:1,$During:{$Left:[0.2,0.8],$Zoom:[0.2,0.8],$Rotate:[0.2,0.8]},$Easing:{$Left:$Jease$.$Swing,$Zoom:$Jease$.$Swing,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$Swing},$Opacity:2,$Round:{$Rotate:0.5}},
            {$Duration:1000,x:-4,$Zoom:11,$Rotate:1,$SlideOut:true,$Easing:{$Left:$Jease$.$InExpo,$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InExpo},$Opacity:2,$Round:{$Rotate:0.8}},
            {$Duration:1200,x:-0.6,$Zoom:1,$Rotate:1,$During:{$Left:[0.2,0.8],$Zoom:[0.2,0.8],$Rotate:[0.2,0.8]},$Easing:{$Left:$Jease$.$Swing,$Zoom:$Jease$.$Swing,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$Swing},$Opacity:2,$Round:{$Rotate:0.5}},
            {$Duration:1000,x:4,$Zoom:11,$Rotate:1,$SlideOut:true,$Easing:{$Left:$Jease$.$InExpo,$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InExpo},$Opacity:2,$Round:{$Rotate:0.8}},
            {$Duration:1200,x:0.5,y:0.3,$Cols:2,$Zoom:1,$Rotate:1,$Assembly:2049,$ChessMode:{$Column:15},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$OutQuad,$Rotate:$Jease$.$InCubic},$Opacity:2,$Round:{$Rotate:0.7}},
            {$Duration:1000,x:0.5,y:0.3,$Cols:2,$Zoom:1,$Rotate:1,$SlideOut:true,$Assembly:2049,$ChessMode:{$Column:15},$Easing:{$Left:$Jease$.$InExpo,$Top:$Jease$.$InExpo,$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InExpo},$Opacity:2,$Round:{$Rotate:0.7}},
            {$Duration:1200,x:-4,y:2,$Rows:2,$Zoom:11,$Rotate:1,$Assembly:2049,$ChessMode:{$Row:28},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$OutQuad,$Rotate:$Jease$.$InCubic},$Opacity:2,$Round:{$Rotate:0.7}},
            {$Duration:1200,x:1,y:2,$Cols:2,$Zoom:11,$Rotate:1,$Assembly:2049,$ChessMode:{$Column:19},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$OutQuad,$Rotate:$Jease$.$InCubic},$Opacity:2,$Round:{$Rotate:0.8}}
        ];

        var jssor_1_options = {
            $AutoPlay: true,
            $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
            },
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
            },
            $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Rows: 2,
                $Cols: 6,
                $SpacingX: 14,
                $SpacingY: 12,
                $Orientation: 2,
                $Align: 156
            }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

        /*responsive code begin*/
        /*remove responsive code if you don't want the slider scales while window resizing*/
        function ScaleSlider() {
            var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
            if (refSize) {
                refSize = Math.min(refSize, 1124);
                refSize = Math.max(refSize, 300);
                jssor_1_slider.$ScaleWidth(refSize);
            }
            else {
                window.setTimeout(ScaleSlider, 30);
            }
        }
        ScaleSlider();
        $Jssor$.$AddEvent(window, "load", ScaleSlider);
        $Jssor$.$AddEvent(window, "resize", ScaleSlider);
        $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
        /*responsive code end*/
    };
</script>
<div class="center col-lg-10 col-lg-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
    <h1 class="modern_title" style="margin-left:0">
        GALLERY
    </h1>
</div>
<div class="center col-lg-10 col-lg-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
    <div id="jssor_1" class="container" style="position:relative;
                        margin:0 auto;top:0px;left:0px;width:1124px;height:480px;overflow:hidden;visibility:hidden;background-color:#ececec;">
        <!-- Loading Screen -->
        <div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('<?=base_url()?>assets/img/slider/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:240px;width:884px;height:480px;overflow:hidden;">
            <div>
                <img data-u="image" src="<?=base_url()?>assets/img/slider/01.jpg" />
                <img data-u="thumb" src="<?=base_url()?>assets/img/slider/thumb-01.jpg" />
            </div>
            <div>
                <img data-u="image" src="<?=base_url()?>assets/img/slider/02.jpg" />
                <img data-u="thumb" src="<?=base_url()?>assets/img/slider/thumb-02.jpg" />
            </div>
            <div>
                <img data-u="image" src="<?=base_url()?>assets/img/slider/03.jpg" />
                <img data-u="thumb" src="<?=base_url()?>assets/img/slider/thumb-03.jpg" />
            </div>
            <div>
                <img data-u="image" src="<?=base_url()?>assets/img/slider/04.jpg" />
                <img data-u="thumb" src="<?=base_url()?>assets/img/slider/thumb-04.jpg" />
            </div>
            <div>
                <img data-u="image" src="<?=base_url()?>assets/img/slider/05.jpg" />
                <img data-u="thumb" src="<?=base_url()?>assets/img/slider/thumb-05.jpg" />
            </div>
            <div>
                <img data-u="image" src="<?=base_url()?>assets/img/slider/06.jpg" />
                <img data-u="thumb" src="<?=base_url()?>assets/img/slider/thumb-06.jpg" />
            </div>
            <div>
                <img data-u="image" src="<?=base_url()?>assets/img/slider/07.jpg" />
                <img data-u="thumb" src="<?=base_url()?>assets/img/slider/thumb-07.jpg" />
            </div>
            <div>
                <img data-u="image" src="<?=base_url()?>assets/img/slider/08.jpg" />
                <img data-u="thumb" src="<?=base_url()?>assets/img/slider/thumb-08.jpg" />
            </div>
            <a data-u="any" href="http://www.jssor.com" style="display:none">Image Gallery with Vertical Thumbnail</a>
            <div>
                <img data-u="image" src="<?=base_url()?>assets/img/slider/09.jpg" />
                <img data-u="thumb" src="<?=base_url()?>assets/img/slider/thumb-09.jpg" />
            </div>
            <div>
                <img data-u="image" src="<?=base_url()?>assets/img/slider/10.jpg" />
                <img data-u="thumb" src="<?=base_url()?>assets/img/slider/thumb-10.jpg" />
            </div>
            <div>
                <img data-u="image" src="<?=base_url()?>assets/img/slider/11.jpg" />
                <img data-u="thumb" src="<?=base_url()?>assets/img/slider/thumb-11.jpg" />
            </div>
            <div>
                <img data-u="image" src="<?=base_url()?>assets/img/slider/12.jpg" />
                <img data-u="thumb" src="<?=base_url()?>assets/img/slider/thumb-12.jpg" />
            </div>
            <div>
                <img data-u="image" src="<?=base_url()?>assets/img/slider/13.jpg" />
                <img data-u="thumb" src="<?=base_url()?>assets/img/slider/thumb-13.jpg" />
            </div>
            <div>
                <img data-u="image" src="<?=base_url()?>assets/img/slider/14.jpg" />
                <img data-u="thumb" src="<?=base_url()?>assets/img/slider/thumb-14.jpg" />
            </div>
        </div>
        <!-- Thumbnail Navigator -->
        <div data-u="thumbnavigator" class="jssort01-99-66" style="position:absolute;left:0px;top:0px;width:240px;height:480px;" data-autocenter="2">
            <!-- Thumbnail Item Skin Begin -->
            <div data-u="slides" style="cursor: default;">
                <div data-u="prototype" class="p">
                    <div class="w">
                        <div data-u="thumbnailtemplate" class="t"></div>
                    </div>
                    <div class="c"></div>
                </div>
            </div>
            <!-- Thumbnail Item Skin End -->
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora05l" style="top:0px;left:248px;width:40px;height:40px;" data-autocenter="2"></span>
        <span data-u="arrowright" class="jssora05r" style="top:0px;right:8px;width:40px;height:40px;" data-autocenter="2"></span>
    </div>
    <h1 class="modern_title" style="margin-left:0"></h1>
</div>
<script type="text/javascript">jssor_1_slider_init();</script>