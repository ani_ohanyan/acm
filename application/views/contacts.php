<div class="center col-lg-10 col-lg-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
    <div id="map"></div>
    <div id="contact_info">
        <div style="width: 32.3%; height: 472px; float: left">
            <h2 class="modern_title" style="margin-left: 0;">Contact info</h2>
            <h5>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.
            </h5>
            <p>
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque la udantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia.<br>
            </p>
            <address>
                <strong>
                    ACM Transporti srl <br>
                    9870 St Vincent Place,<br>
                    Glasgow, DC 45 Fr 45.
                </strong> <br>
                Telephone: +1 800 603 6035<br>
                FAX: +1 800 889 9898<br>
                E-mail: <a href="mailto:info@demolink.org">mail@demolink.org</a><br>
            </address>
        </div> 
        <div style="float:left; margin-left: 2%; width: 65.7%">
            <h2 class="modern_title" style="margin-left: 0;">Contact form</h2>
			<form action="" method="post" >
                <input class="contact_inputs" type="text" id="msg_name" value="" size="30" placeholder="Name:">
                <input class="contact_inputs" type="email" id="msg_email" size="20" placeholder="E-mail:">
                <input class="contact_inputs" type="text" id="msg_phone" size="40" placeholder="Phone:"><br>
                <textarea class="contact_inputs" id="msg_message" cols="40" rows="10"  placeholder="Message:"></textarea><br>
                <input class="contact_inputs" type="reset" value="clear" ></input>
                <input class="contact_inputs" type="submit" value="send" id="send_msg" name="send" ></input>
			</form>
        </div>
<!--        <div id="worning"></div>-->
    </div>
</div>

<script>
   function initMap() {
        var myLatLng = {lat: 45.1657558, lng: 7.9560406};

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 13,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Hello World!'
        });
      }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB--17CPUOop6caL1-Z9QU2LGmqDWFJqTw&callback=initMap" async defer></script>