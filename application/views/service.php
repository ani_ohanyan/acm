<div class="center col-lg-10 col-lg-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
    <div class="center">
        <div class="title_section" style="text-align: right; padding: 15px 0 30px; border: 0">
            <span class="about" style="padding-right: 70px"> CATEGORIES: </span>
            <a href="?category=0">
                <span id='current_category' class="about category" style="padding-right: 70px; color: #464646;"> SHOW ALL </span>
            </a>
            <a href="?category=1">
                <span class="about category" style="padding-right: 70px; color: #464646;"> CATEGORY 1 </span>
            </a>
            <a href="?category=2">
                <span class="about category" style="color: #464646"> CATEGORY 2 </span>
            </a>
        </div>
        <div class="cat_page">
            <div class="cat_elem float_l">
                <div class="work_img_div zoom modern_img" data-toggle="modal" data-target="#imgModal">
                    <img src="<?=base_url()?>assets/img/img_1.jpg">
                </div>
                <div class="work_texts">
                    <h3>What is Lorem Ipsum</h3>
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </p>
                </div>
                <div id="more_info_about">
                    <div class="more_info more_works">
                        <span> SHOW MORE </span>
                    </div>
                </div>
            </div>

            <div class="cat_elem float_l">
                <div class="work_img_div zoom modern_img" data-toggle="modal" data-target="#imgModal">
                    <img src="<?=base_url()?>assets/img/img_1.jpg">
                </div>
                <div class="work_texts">
                    <h3>Why do we use it</h3>
                    <p>
                        It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                    </p>
                </div>
                <div id="more_info_about">
                    <div class="more_info more_works">
                        <span> SHOW MORE </span>
                    </div>
                </div>
            </div>

            <div class="cat_elem float_l">
                <div class="work_img_div zoom modern_img" data-toggle="modal" data-target="#imgModal">
                    <img src="<?=base_url()?>assets/img/img_1.jpg">
                </div>
                <div class="work_texts">
                    <h3>Where can I get some?</h3>
                    <p>
                        There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.
                    </p>
                </div>
                <div id="more_info_about">
                    <div class="more_info more_works">
                        <span> SHOW MORE </span>
                    </div>
                </div>
            </div>

        </div>
        <div id="paging">
            <a href=?page='.$i.'> <span  id="current_page" >1</span> </a>
            <a href=?page='.$i.'&category='.$category.'> <span>2</span> </a>
        </div>
    </div>
</div>