<!-------------SLIDE--------------->
<div id="slide_container">
	<div id="slide_imgs"><!--data-scroll-speed="10"-->
		<img id="slide_img" /> <!--data-image-src=""-->
	</div>
</div>
<!---------------------------->
<section class="center col-lg-10 col-lg-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
	<div id="slide">
		<div id="slide_left" class="col-lg-1 col-xs-1"> </div>
		<p id="slide_title" class="col-lg-10 col-xs-10"> </p>
		<div id="slide_right" class="col-lg-1 col-xs-1"> </div>
	</div>
</section>
<section class="col-lg-10 col-lg-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
    <div id="second">
        <h1>
            AFFORDABLE SOLUTIONS FOR BETTER LIVING. STYLE, COMFORT, QUALITY, SELECTION!
        </h1>
    </div>
</section>
<section class="col-lg-12 col-xs-12 grey_section">
    <div class="col-lg-10 col-lg-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 no_padding">
        <div class="col-lg-4 col-sm-4">
            <div class="modern">
                <div class="modern_img zoom modern_img1"  data-toggle="modal" data-target="#imgModal">
                    <img src="<?=base_url()?>assets/img/img_1.jpg" />
                </div>
                <div>
                    <h1 class="modern_title">
                        <a href="#">MODERN</a>
                    </h1>
                    <h3>
                        MES CUML DIA SED INENIAS INGER LOT ALIIQ DOLORE.
                    </h3>
                    <h4>
                        Aliquam dapibus tincidunt metus. Praesent justo dolor, lobortis quis, lobortis dignissim, pulvinar ac, lorem. Lorem...
                    </h4>
                </div>
                <div class="more_container">
                    <a href="#">
                        <div class="more_info more" >
                            <span> MORE INFO </span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-4">
            <div class="modern">
                <div class="modern_img zoom modern_img1"  data-toggle="modal" data-target="#imgModal">
                    <img src="<?=base_url()?>assets/img/img_2.jpg" />
                </div>
                <div class="bg_white">
                    <h1 class="modern_title">
                        <a href="#"> COMFORTABLE </a>
                    </h1>
                    <h3>
                        MES CUML DIA SED INENIAS INGER LOT ALIIQ DOLORE.
                    </h3>
                    <h4>
                        Aliquam dapibus tincidunt metus. Praesent justo dolor, lobortis quis, lobortis dignissim, pulvinar ac, lorem. Lorem...
                    </h4>
                </div>
                <div class="more_container">
                    <a href="#">
                        <div class="more_info more" >
                            <span> MORE INFO </span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-4">
            <div class="modern">
                <div class="modern_img zoom modern_img1" data-toggle="modal" data-target="#imgModal">
                    <img src="<?=base_url()?>assets/img/img_3.jpg"/>
                </div>
                <div class="bg_white">
                    <h1 class="modern_title">
                        <a href="#">PRESTIGE</a>
                    </h1>
                    <h3>
                        MES CUML DIA SED INENIAS INGER LOT ALIIQ DOLORE.
                    </h3>
                    <h4>
                        Aliquam dapibus tincidunt metus. Praesent justo dolor, lobortis quis, lobortis dignissim, pulvinar ac, lorem. Lorem...
                    </h4>
                </div>
                <div class="more_container">
                    <a href="#">
                        <div class="more_info more" >
                            <span> MORE INFO </span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!---------------------------------------------->
<section class="center col-lg-10 col-lg-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0">
	<div id="round_abs">
		<h1 class="modern_title"> FEATURED PROJECTS </h1>
		<div>
			<div class="round_container col-lg-3 col-md-3 col-sm-6 col-xs-6" style="margin-left:0">
				<div class="round zoom" data-toggle="modal" data-target="#imgModal">
					<img src="<?=base_url()?>assets/img/img_4.jpg" class="round_imgs" title="Sodales orci et" />
					<span class="zoom_icon"></span>
				</div>
				<span class="round_title"> Sodales orci et </span>
			</div>
			<div class="round_container col-lg-3 col-md-3 col-sm-6 col-xs-6">
				<div class="round zoom" data-toggle="modal" data-target="#imgModal">
					<img src="<?=base_url()?>assets/img/img_5.jpg" class="round_imgs" title="IN FAUCIBUS RISUS" />
					<span class="zoom_icon"></span>
				</div>
				<span class="round_title"> IN FAUCIBUS RISUS </span>
			</div>
			<div class="round_container col-lg-3 col-md-3 col-sm-6 col-xs-6">
				<div class="round zoom" data-toggle="modal" data-target="#imgModal">
					<img src="<?=base_url()?>assets/img/img_6.jpg" class="round_imgs" title="In Fauctibus" />
					<span class="zoom_icon"></span>
				</div>
				<span class="round_title"> In Fauctibus </span>
			</div>
			<div class="round_container col-lg-3 col-md-3 col-sm-6 col-xs-6">
				<div class="round zoom" data-toggle="modal" data-target="#imgModal">
					<img src="<?=base_url()?>assets/img/img_7.jpg" class="round_imgs" title="Malesuada fames ac" />
					<span class="zoom_icon"></span>
				</div>
				<span class="round_title"> Malesuada fames ac </span>
			</div>
		</div>
	</div>
</section>

<!---------------------------------------------->
<!--	<section class="center" id="block_2">
        <div id="why_us" class="float_l">
            <div id="why_us_abs">
                <h1 class="modern_title" style="margin-left:0">
                    WHY US?
                </h1>
                <img src="<?=base_url()?>assets/img/pic_4.jpg" />
                <h2>
                    MES CUML DIA SED INENSI   LOT ALIIQT DOLOR IPSUM.
                </h2>
                <p>
                    Nam justo ante, hendrerit vitae aliquet condimentum, commodo eu mi. Etiam sol licitudin odio vehicula venenatis. Aenean ac pulvinar nisi. Aliquam dui dolor.
                </p>
            <div>
        </div>
        <div id="what_new" class="float_l">
        </div>
        <div id="about_us" class="float_l">
        </div>
    </section>-->